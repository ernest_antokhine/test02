/**
 * 
 */
package dbhelpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Client;

/**
 * @author Ernest
 *
 */
public class AddQuery {
	
	// connection
	public Connection connection;
	
	// constructor
	public AddQuery(String dbName, String user, String pwd){
		
		String url = "jdbc:mysql://localhost:3306/" + dbName;
		
		// activate driver
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			// activate connection
			this.connection = DriverManager.getConnection(url, user, pwd);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void doAdd(Client client){
		
		String query = "insert into client (f_name, m_name,l_name, subject, message, client_manager_id) values (?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = connection.prepareStatement(query);
			
			// get values for tokens
			ps.setString(1, client.getF_name());
			ps.setString(2, client.getM_name());
			ps.setString(3, client.getL_name());
			ps.setString(4, client.getSubject());
			ps.setString(5, client.getMessage());
			ps.setInt(6, client.getManagerId());
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
