/**
 * 
 */
package dbhelpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Client;
import model.Manager;

/**
 * @author Ernest
 *
 */
public class ReadQuery {
	
	private Connection connection;
	private ResultSet results;
	
	public ReadQuery(String dbName, String user, String pwd){
		
		String url = "jdbc:mysql://localhost:3306/" + dbName;
		
		// set up driver
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			// use this driver to establish connection
			this.connection = DriverManager.getConnection(url, user, pwd);
			
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// fetch the data
	public void doRead(){
		
		String query = "select c.f_name, c.m_name, c.l_name, subject, message, m.f_name, m.m_name, m.l_name from client c left join manager m on client_manager_id = manager_id order by client_id";
		try {
			PreparedStatement ps = this.connection.prepareStatement(query);
			
			this.results = ps.executeQuery();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// organize the data in the form of a HTML table
	public String getHTMLTable(){
		
		String table = "";
		
		table += "<table border=1>";
		
		try {
			while(this.results.next()){
				Client client = new Client();
				Manager manager = new Manager();
				
				client.setF_name(this.results.getString("c.f_name"));
				client.setM_name(this.results.getString("c.m_name"));
				client.setL_name(this.results.getString("c.l_name"));
				client.setSubject(this.results.getString("subject"));
				client.setMessage(this.results.getString("message"));
				manager.setF_name(this.results.getString("m.f_name"));
				manager.setM_name(this.results.getString("m.m_name"));
				manager.setL_name(this.results.getString("m.l_name"));
				
				
				/*
				client.setF_name(this.results.getString(1));
				client.setM_name(this.results.getString(2));
				client.setL_name(this.results.getString(3));
				client.setSubject(this.results.getString(4));
				client.setMessage(this.results.getString(5));
				// manager.setF_name(this.results.getString(6));
				// manager.setF_name(this.results.getString(7));
				manager.setF_name(this.results.getString(6));
				*/
				
				table += "<tr>";
				table += "<td>";
					table += client.getF_name();
				table += "</td>";
				table += "<td>";
					table += client.getM_name();
				table += "</td>";
				table += "<td>";
					table += client.getL_name();
				table += "</td>";
				table += "<td>";
					table += client.getSubject();
				table += "</td>";
				table += "<td>";
					table += client.getMessage();
				table += "</td>";
				
				table += "<td>";
					table += "<label>Manager:</label>";
				table += "</td>";
				
				table += "<td>";
					table += manager.getF_name();
				table += "</td>";
				table += "<td>";
					table += manager.getM_name();
				table += "</td>";
				 
				table += "<td>";
					table += manager.getL_name();
				table += "</td>";
				table += "</tr>";
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		table += "</table>";
		return table;
	}

}
