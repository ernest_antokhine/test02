/**
 * 
 */
package model;

/**
 * @author Ernest
 *
 */
public class Client {
	
	// private members
	//private int book_id;
	private String f_name;
	private String m_name;
	private String l_name;
	private String subject;
	private String message;
	private int manager_id;
	
	//constructors
	
	public Client() {
		
		this.f_name = "Unknown";
		this.m_name = "Unknown";
		this.l_name = "Unknown";
		this.subject = "Unknown";
		this.message = "Unknown";
		this.manager_id = 0;
	}
	/**
	 * @param f_name
	 * @param m_name
	 * @param l_name
	 * @param subject
	 * @param message
	 */
	public Client(String f_name, String m_name, String l_name, String subject, String message, int manager_id) {
		
		this.f_name = f_name;
		this.m_name = m_name;
		this.l_name = l_name;
		this.subject = subject;
		this.message = message;
		this.manager_id = manager_id;
	}
	
	// getters and setters
	/**
	 * @return the f_name
	 */
	public String getF_name() {
		return f_name;
	}
	/**
	 * @param f_name the f_name to set
	 */
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}
	/**
	 * @return the m_name
	 */
	public String getM_name() {
		return m_name;
	}
	/**
	 * @param m_name the m_name to set
	 */
	public void setM_name(String m_name) {
		this.m_name = m_name;
	}
	/**
	 * @return the l_name
	 */
	public String getL_name() {
		return l_name;
	}
	/**
	 * @param l_name the l_name to set
	 */
	public void setL_name(String l_name) {
		this.l_name = l_name;
	}
	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	
	public int getManagerId(){
		return manager_id; 
	}
	
	public void setManagerId(int manager_id){
		this.manager_id = manager_id;; 
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Client [f_name=" + f_name + ", m_name=" + m_name + ", l_name=" + l_name + ", subject=" + subject
				+ ", message=" + message + ", manager_id=" + manager_id + "]";
	}
	
	
	
	
	
	
	
	
	

}
