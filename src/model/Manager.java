/**
 * 
 */
package model;

/**
 * @author Ernest
 *
 */
public class Manager {
	private String f_name;
	private String m_name;
	private String l_name;
	
	/**
	 * @param f_name
	 * @param m_name
	 * @param l_name
	 */
	public Manager(String f_name, String m_name, String l_name) {
		super();
		this.f_name = f_name;
		this.m_name = m_name;
		this.l_name = l_name;
	}
	
	public Manager() {
		super();
		this.f_name = "";
		this.m_name = "";
		this.l_name = "";
	}

	/**
	 * @return the f_name
	 */
	public String getF_name() {
		return f_name;
	}

	/**
	 * @param f_name the f_name to set
	 */
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	/**
	 * @return the m_name
	 */
	public String getM_name() {
		return m_name;
	}

	/**
	 * @param m_name the m_name to set
	 */
	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	/**
	 * @return the l_name
	 */
	public String getL_name() {
		return l_name;
	}

	/**
	 * @param l_name the l_name to set
	 */
	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Manager [f_name=" + f_name + ", m_name=" + m_name + ", l_name=" + l_name + "]";
	}
	
	
	
	

}
