package controllers;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbhelpers.AddQuery;
import model.Client;

/**
 * Servlet implementation class AddServlet
 */
@WebServlet(description = "Controller for adding the new client and comments to the db", urlPatterns = { "/addClient" })
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// get the data
		String f_name = request.getParameter("f_name");
		String m_name = request.getParameter("m_name");
		String l_name = request.getParameter("l_name");
		String subject = request.getParameter("subject");
		String comment = request.getParameter("comment");
		int manager_id = Integer.parseInt(request.getParameter("managers"));  
		
		// set up the Client object, set values
		Client client = new Client();
		client.setF_name(f_name);
		client.setM_name(m_name);
		client.setL_name(l_name);
		client.setSubject(subject);
		client.setMessage(comment);
		client.setManagerId(manager_id);
		
		// set up an AddQuery object
		AddQuery aq = new AddQuery("testdb_2","root","mysql");
		
		// pass the client to AddQuery to add to the db
		aq.doAdd(client);
		
		// pass execution control to the read servlet
		String url = "/read";
		RequestDispatcher dispatcher = request.getRequestDispatcher(url);
		dispatcher.forward(request, response);
		
		
		
		
	}

}
